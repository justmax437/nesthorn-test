package db

import (
	_ "github.com/mattn/go-sqlite3"
	"database/sql"
	"log"
	"errors"
)

var (
	dbHandle *sql.DB
	countStmt *sql.Stmt
	countSameUidStmt *sql.Stmt
	ipQueryStmt *sql.Stmt
)

const (
	driver = "sqlite3"
	dsn = `file:./db.sqlite?cache_size=2000&default_cache_size=2000&count_changes=false&page_size=1024&temp_store=MEMORY&synchronous=OFF`
)

const (
	countQuery = `SELECT COUNT(DISTINCT  uid) FROM log WHERE ip IS ? AND uid IN(?,?)`
	ipQuery = `SELECT DISTINCT ip FROM log WHERE uid IN(?,?)`
)

func Connect() error {
	return connect()
}

func connect() error {
	var err error
	dbHandle, err = sql.Open(driver, dsn)
	if err != nil {
		log.Fatalln(err)
	}

	if err = dbHandle.Ping(); err != nil {
		log.Fatalln(err)
	}

	countStmt, err = dbHandle.Prepare(countQuery)
	if err != nil {
		log.Fatalln(err)
	}

	ipQueryStmt, err = dbHandle.Prepare(ipQuery)
	if err != nil {
		log.Fatalln(err)
	}

	return nil
}

func Close() {
	countStmt.Close()
	ipQueryStmt.Close()
	dbHandle.Close()
}

func DupeCheck(uid1, uid2 int64) (bool, error) {
	if uid1 == uid2 {
		return false, errors.New("uids cannot be equal")
	}

	rows, err := ipQueryStmt.Query(uid1, uid2)
	if err != nil {
		log.Println(err)
		return false, err
	}
	defer rows.Close()

	ips := make([]string, 0, 2)
	for rows.Next() {
		var tmp string
		err = rows.Scan(&tmp)
		if err != nil {
			return false, err
		}
		ips = append(ips, tmp)
	}
	rows.Close()

	for _, ipaddr := range ips {
		var count int64
		rows, err = countStmt.Query(ipaddr, uid1, uid2)
		if err != nil {
			log.Fatalln(err)
			return false, err
		}
		rows.Next()
		if err = rows.Scan(&count); err == nil {
			if count > 1 {
				return true, nil
			}
		} else {
			log.Println(err)
			return false, err
		}

	}
	return false, nil
}
