package main

import (
	"testtask/server"
	"flag"
	"testtask/db"
	"os/signal"
	"os"
	"sync"
)

var once sync.Once

func init() {
	ch := make(chan os.Signal)
	signal.Notify(ch, os.Kill, os.Interrupt)
	go func(ch <-chan os.Signal) {
		<-ch
		once.Do(interruptHandler)
	}(ch)
}

func main() {
	flag.StringVar(&server.Addr, "sockAddr", "localhost:12345", "Address to listen for request")
	flag.Parse()

	defer once.Do(interruptHandler)

	db.Connect()
	server.Start()
}


func interruptHandler() {
	server.Stop()
	db.Close()
}