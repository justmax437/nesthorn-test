package server

import "fmt"

type checkResult struct{
	Dupes  bool   `json:"dupes"`
	Desc   string `json:"error_description"`
}

func (r checkResult) Error() string {
	return r.Desc
}

func (r checkResult) MarshalJSON() ([]byte, error) {
	return []byte(r.JsonStr()), nil
}

func (r checkResult) JsonStr() string {
	if r.Desc != "" {
		return fmt.Sprintf(`{"error": "%s"}`, r.Desc)
	} else {
		return fmt.Sprintf(`{"dupes": %v}`, r.Dupes)
	}
}

