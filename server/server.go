package server

import (
	"net/http"
	"time"
	"log"
	"regexp"
	"strconv"
	"testtask/db"
	"io"
	"context"
)

var (
	urlPathRegex = regexp.MustCompile("^(/)?[0-9]+/[0-9]+(/)?$")
	uidRegex = regexp.MustCompile("[0-9]+")
)

var Addr string

var serv http.Server

func Start() {
	setupServeMux()
	serv = http.Server{
		Addr: Addr,
		ReadTimeout: time.Second * 3,
		WriteTimeout: time.Second * 2,
	}
	err := serv.ListenAndServe()
	if http.ErrServerClosed == err {
		return
	} else {
		log.Fatalln(err)
	}
}

func Stop() {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 5)
	defer cancel()
	serv.Shutdown(ctx)
}

func setupServeMux() {
	reqHandle := func(out http.ResponseWriter, r *http.Request) {
		out.Header().Set("Content-Type", "application/json")
		if !urlPathRegex.MatchString(r.URL.Path) {
			out.WriteHeader(http.StatusBadRequest)
			io.WriteString(out, checkResult{false, "Malformed URL"}.JsonStr())
			return
		}

		uid1, uid2, err := parseUids(r.URL.Path)
		if err != nil {
			out.WriteHeader(http.StatusBadRequest)
			io.WriteString(out, checkResult{false, "Failed to parse UIDs"}.JsonStr())
			return
		}

		dupes, err := db.DupeCheck(uid1, uid2)
		if err != nil {
			out.WriteHeader(http.StatusInternalServerError)
			io.WriteString(out, checkResult{false, "Check failed"}.JsonStr())
			return
		}
		io.WriteString(out, checkResult{dupes, ""}.JsonStr())
	}

	http.HandleFunc("/", reqHandle)
}

func parseUids(path string) (int64, int64, error) {
	matches := uidRegex.FindAllString(path, 2)
	uid1, err := strconv.ParseInt(matches[0], 10, 64)
	if err != nil {
		return 0,0,err
	}
	uid2, err := strconv.ParseInt(matches[1], 10, 64)
	if err != nil {
		return 0,0,err
	}
	return uid1, uid2, nil
}